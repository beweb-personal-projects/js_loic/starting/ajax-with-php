window.onload = () => {
    function getAdminsFromServer(cb) {
        fetch('http://localhost:666/php/functions/generics.php').then((res) => {
            res.json().then((data) => {
                cb(data);
            })
        })
    }

    function createAdminsInterface() {
        var parentDiv = document.querySelector(".parentDiv");
        let rowAdminsTemplate = document.querySelector('#rowadmins');

        getAdminsFromServer((data) => {
            data.forEach(admin => {
                let clone = rowAdminsTemplate.content.cloneNode(true);
                let heading_text = clone.querySelectorAll("h6");
                let details_text = clone.querySelectorAll("p");

                heading_text[0].textContent = admin.name;
                details_text[0].textContent = `Administrator Level: ${admin.level}`;

                if(admin.ignorePerms) {
                    let ignore_text = clone.querySelectorAll("small");
                    
                    ignore_text[0].textContent = "Ignore Perms";
                }

                parentDiv.appendChild(clone);
            });
        })
    }

    createAdminsInterface();
}