<?php 

$all_admins = [
    [
        "name" => "strackz1",
        "level" => 20,
        "ignorePerms" => false,
    ],
    [
        "name" => "strackz2",
        "level" => 30,
        "ignorePerms" => false,
    ],
    [
        "name" => "strackz3",
        "level" => 1,
        "ignorePerms" => true,
    ],
    [
        "name" => "strackz4",
        "level" => 15,
        "ignorePerms" => false,
    ],
    [
        "name" => "strackz5",
        "level" => 10,
        "ignorePerms" => true,
    ]
];
