<?php 

include_once "./data.php";

function getAdminsArray($cb, $admins_data) {
    if(count($admins_data) > 0) {
        $cb($admins_data);
    }
}

getAdminsArray(function($data) {
    echo json_encode($data);
}, $all_admins);